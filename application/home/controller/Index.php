<?php
namespace app\home\controller;

class Index extends Base
{
    public function index()
    {
        /*第三方登录*/
        $code=input('get.code');
        if(!empty($code)) {
            import('ThinkSDK.ThinkOauth');
            $login_type = session('login_type');
            switch ($login_type) {
                case 'qq':
                    $sns = \Org\ThinkSDK\ThinkOauth::getInstance('QQ');
                    $extend = null;
                    $token = $sns->getAccessToken($code, $extend);
                    if (is_array($token)) {
                        //判断用户是否存在
                        if(!empty($token['openid'])){
                            $this->autoLogin('third_id',$token['openid']);
                        }
                        //否则获取用户信息注册登录
                        $data = $sns->call('user/get_user_info');
                        if ($data['ret'] == 0) {
                            if($data['gender']=='男'){
                                $userInfo['gender'] =1 ;
                            }elseif($data['gender']=='女'){
                                $userInfo['gender'] =2 ;
                            }else{
                                $userInfo['gender']=0;
                            }
                            $userInfo['username'] = $data['nickname'];
                            $userInfo['avatar'] = $data['figureurl_qq_2'];
                            $userInfo['province']=$data['province'];
                            $userInfo['city']=$data['city'];
                            $userInfo['third_id']=$token['openid'];
                            //插入用户数据
                            $reg_result = model('Member')->autoReg($userInfo);
                            if($reg_result['error_code']){
                                $this->returnJson($reg_result['msg'],0);
                            }else{
                                $login_result = model('Member')->autoLogin('third_id',$userInfo['third_id']);
                                if($login_result['error_code']){
                                    $this->returnJson($login_result['msg'],0);
                                }else{
                                    $now_user = $login_result['user'];
                                    session('user',$now_user);
                                    $this->returnJson('登录成功！',1);
                                    exit;
                                }
                            }
                        }else{
                            $this->returnJson('获取用户信息失败！',0);
                        }
                    }else{
                        $this->returnJson('获取openid失败！',0);
                    }
                    break;
            }
        }
    }

    /**
     * 自动登录
     * @param $field
     * @param $value
     * created by sunnier<xiaoyao_xiao@126.com>
     */
    protected function autoLogin($field,$value){
        $result = model('Member')->autoLogin($field,$value);
        if(empty($result['error_code'])){
            $now_user = $result['user'];
            session('user',$now_user);
            $this->returnJson('登录成功！',1);
        }else if($result['error_code'] && $result['error_code'] != 1001){
            $this->returnJson($result['msg'],0);
        }
    }

    public function loginQq() {
        import('ThinkSDK.ThinkOauth');
        $sdk = \Org\ThinkSDK\ThinkOauth::getInstance('qq');
        session('login_type', 'qq');
        $url=$sdk->getRequestCodeURL();
        return redirect($url);
    }

    public function info(){
        echo phpinfo();
    }

    /**
     * 微信登录异步请求
     * @return \think\response\Json
     * created by sunnier<xiaoyao_xiao@126.com>
     */
    public function ajaxWechatLogin(){
            for ($i = 0; $i < 6; $i++) {
                $database_login_qrcode = model('LoginQrcode');
                $condition_login_qrcode['id'] = input('get.qrcode_id');
                if(empty($condition_login_qrcode['id'])){
                    return $this->returnJson('未获取到qrcode_id！',0);
                }
                $now_qrcode = $database_login_qrcode->field('`uid`')->where($condition_login_qrcode)->find();
                if (!empty($now_qrcode['uid'])) {
                    if ($now_qrcode['uid'] == -1) {
                        $data_login_qrcode['uid'] = 0;
                        $database_login_qrcode->where($condition_login_qrcode)->isUpdate(true)->save($data_login_qrcode);
                        return $this->returnJson('请在微信公众号点击授权登录！',0);
                    }
                    $database_login_qrcode->where($condition_login_qrcode)->delete();
                    $result = model('Member')->autologin('id', $now_qrcode['uid']);
                    if (empty($result['error_code'])) {
                        return $this->returnJson('登录成功！',1,$result['user']);
                    } else if ($result['error_code'] == 1001) {
                        return $this->returnJson('没有查找到用户，请重新扫描二维码！',0);
                    } else if ($result['error_code']) {
                        return $this->returnJson('登陆失败！',0);
                    }
                }
                if ($i == 5) {
                    return $this->returnJson('登陆失败',0);
                }
                sleep(3);
            }
    }
}
