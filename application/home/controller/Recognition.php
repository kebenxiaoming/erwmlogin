<?php
/**
 * Created by PhpStorm.
 * User: sunny
 * 尽人事，听天命
 * Date: 2017/6/25
 * Time: 12:27
 */
namespace app\home\controller;

class Recognition extends Base{

    public function seeLoginQrcode(){
        $qrcode_return = model('Recognition')->getLoginQrcode();
        if($qrcode_return['error_code']){
            return $this->returnJson("获取失败！",0);
        }else{
            $data=array(
                    'url'=>$qrcode_return['ticket'],
                    'qrcode_id'=>$qrcode_return['id'],
            );
            return $this->returnJson("获取成功！",1,$data);
        }
    }
}
