<?php
/**
 * Created by PhpStorm.
 * User: sunny
 * For Darling
 * Date: 2017/6/18
 * Time: 11:24
 */
namespace app\home\controller;

use think\controller\Rest;

class Base extends Rest{
    /**
     * 设置json返回值
     * @param $data
     * @param int $code
     * @param array $header
     * @return \think\response\Json
     */
    public function returnJson($msg,$status,$myData=array(),$code=200,$header=array('Access-Control-Allow-Origin'=>ALLOW_URL,)){
        $data=array(
            'data'=>$myData,
            'msg'=>$msg,
            'status'=>$status,
        );
        return json($data,$code,$header);
    }
    public function error_tips($msg, $url = 'javascript:history.back(-1);')
    {
        $this->assign("msg", $msg);
        $this->assign("url", $url);
        return $this->fetch("Home/error");
        exit();
    }

    public function success_tips($msg, $url = 'javascript:history.back(-1);')
    {
        $this->assign("msg", $msg);
        $this->assign("url", $url);
        return $this->display("Home/success");
        exit();
    }
}