<?php
/**
 * Created by PhpStorm.
 * User: sunny
 * 尽人事，听天命
 * Date: 2017/6/26
 * Time: 23:09
 */
namespace app\common\model;

use think\Model;
class Member extends Model{
    /*手机号、union_id、open_id 直接登录入口*/
    public function autoLogin($field,$value){
        $condition_user[$field] = $value;
        $now_user = $this->field(true)->where($condition_user)->find();
        if($now_user){
            if(empty($now_user['status'])){
                return array('error_code' => true, 'msg' => '该帐号被禁止登录!');
            }
            $data_save_user['id'] = $now_user['id'];
            $data_save_user['last_ip'] = get_client_ip(1);
            $token=getToken($now_user);
            if($token['error']){
                return array('error_code' => true, 'msg' => '获取token失败!');
            }
            $now=time();
            $now_user['token']=$data_save_user['token']=$token['data']['token'];
            $now_user['refresh_token']=$data_save_user['refresh_token']=$token['data']['refresh_token'];
            $data_save_user['token_time']=$now;
            $data_save_user['refresh_token_time']=$now;
            if($this->isUpdate(true)->save($data_save_user)) {
                return array('error_code' => false, 'msg' => 'OK', 'user' => $now_user);
            }else{
                return array('error_code' => true, 'msg' => '更新用户信息失败!');
            }
        }else{
            return array('error_code'=>1001,'msg'=>'没有此用户！');
        }
    }
    public function autoRegLogin($field,$value){
        $condition_user[$field] = $value;
        $now_user = $this->field(true)->where($condition_user)->find();
        if($now_user){
            $now_user=$now_user->toArray();
            if(empty($now_user['status'])){
                return array('error_code' => true, 'msg' => '该帐号被禁止登录!');
            }
            return array('error_code' => false, 'msg' => 'OK', 'user' => $now_user);
        }else{
            return array('error_code'=>true,'msg'=>'没有此用户！');
        }
    }
    /*
	 *	提供用户信息注册用户，密码需要自行md5处理
	 *
	 *	**** 请自行处理逻辑，此处直接插入用户表 ****
	 */
    public function autoReg($data_user){
        $data_user['add_ip'] = $data_user['last_ip'] = get_client_ip(1);
        if($this->save($data_user)){
            return array('error_code' =>false,'msg' =>'OK');
        }else{
            return array('error_code' => true, 'msg' => '注册失败！请重试。');
        }
    }
}