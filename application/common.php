<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/**
 * 获取客户端IP地址
 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
 * @return mixed
 */
function get_client_ip($type = 0) {
    $type       =  $type ? 1 : 0;
    static $ip  =   NULL;
    if ($ip !== NULL) return $ip[$type];
    if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        $pos    =   array_search('unknown',$arr);
        if(false !== $pos) unset($arr[$pos]);
        $ip     =   trim($arr[0]);
    }elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
        $ip     =   $_SERVER['HTTP_CLIENT_IP'];
    }elseif (isset($_SERVER['REMOTE_ADDR'])) {
        $ip     =   $_SERVER['REMOTE_ADDR'];
    }
    // IP地址合法验证
    $long = sprintf("%u",ip2long($ip));
    $ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
    return $ip[$type];
}

/**
 * 获取token
 * @param $user
 * @return array
 * created by sunnier<xiaoyao_xiao@126.com>
 */
function getToken($user){
    if(empty($user)){
        $data=array(
            'error'=>true,
            'msg'=>"用户信息为空！",
            'data'=>'',
        );
        return $data;
    }
    $now=time();
    $token=md5(sha1($user['id'].$now."sunnierandbqc"));
    $refresh_token=md5(sha1($user['id'].$now."sunnierrefreshbqc"));
    $data = array(
        'error' => false,
        'msg' => "获取成功！",
        'data' => array(
            'token'=>$token,
            'refresh_token'=>$refresh_token,
        ),
    );
    return $data;
}

/**
 * 自定义获取post数据方法
 * @return bool|mixed
 * created by sunnier<xiaoyao_xiao@126.com>
 */
function post(){
    $input = file_get_contents("php://input",true);
    $res=false;
    if(!empty($input)&&is_string($input)) {
        $res = json_decode($input, true);
    }
    if($res){
        return $res;
    }else{
        return false;
    }
}