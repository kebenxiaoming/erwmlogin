<?php
/**
 * Created by PhpStorm.
 * User: sunny
 * For Darling
 * Date: 2017/6/18
 * Time: 11:24
 */
namespace app\mobile\controller;

use think\controller\Rest;

class Base extends Rest{
    //判断是否是微信浏览器
    public $is_wexin_browser = false;
    public function __construct()
    {
        parent::__construct();
        if (strpos($_SERVER["HTTP_USER_AGENT"], "MicroMessenger") !== false) {
            $this->is_wexin_browser = true;
        }
    }
    /**
     * 设置json返回值
     * @param $data
     * @param int $code
     * @param array $header
     * @return \think\response\Json
     */
    public function returnJson($msg,$status,$myData=array(),$code=200,$header=array('Access-Control-Allow-Origin'=>ALLOW_URL,)){
        $data=array(
            'data'=>$myData,
            'msg'=>$msg,
            'status'=>$status,
        );
        return json($data,$code,$header);
    }
    public function error_tips($msg)
    {
        $this->returnJson($msg,0);
    }

    public function success_tips($msg)
    {
        $this->returnJson($msg,1);
    }
}