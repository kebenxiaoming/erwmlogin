<?php
/**
 * Created by PhpStorm.
 * User: sunny
 * 尽人事，听天命
 * Date: 2017/6/25
 * Time: 13:46
 */
namespace app\mobile\controller;

class Wechat extends Base{

    public function index()
    {
        import('Wechat.Wechat');
        $wechat = new \Wechat();
        $data = $wechat->request();
        list($content, $type) = $this->reply($data);
        if ($content) {
            $wechat->response($content, $type);
        }
        else {
            exit();
        }
    }
    public function reply($data)
    {
        if ($data['MsgType'] == 'event') {
            $id = $data['EventKey'];
            switch (strtoupper($data['Event'])) {
                case 'SCAN':
                    return $this->scan($id, $data['FromUserName']);
                case 'CLICK':
                    //回复？
                    return array('click', 'text');
                    break;
                case 'SUBSCRIBE':
                    //关注
                    return array('Welcome', 'text');
                    break;
                case 'UNSUBSCRIBE':
                    //取关

                    return array('BYE-BYE', 'text');
                case 'LOCATION':
                    //定位

                    break;
            }
        }
        else {
            if ($data['MsgType'] == 'text') {
                return array("测试成功！",'text');
            }

            if ($data['MsgType'] == 'location') {

            }

            if (import('@.ORG.' . $data['MsgType'] . 'MessageReply')) {

            }
        }

        return false;
    }
    private function scan($id, $openid = '', $issubscribe = 0)
    {
        if ((1000000000 < $id) && $openid) {
             if ($user = model('Member')->field('id')->where(array('third_id' => $openid))->find()) {
                 $data=array(
                     'id'=>$id,
                     'uid'=> $user['id']
                 );
                 model('LoginQrcode')->isUpdate()->save($data);
                 return array('登陆成功', 'text');
             }
             $data=array(
                 'id'=>$id,
                 'uid'=>-1
             );
             model('LoginQrcode')->isUpdate(true)->save($data);
            $return[] = array('点击授权登录', '',config('SITE_LOGO'), config('SITE_URL') . '/mobile/WechatBind/ajaxWebLogin?qrcode_id=' . $id);
            return array($return, 'news');
        }
    }
}
