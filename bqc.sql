-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2017-07-05 16:51:57
-- 服务器版本： 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bqc`
--

-- --------------------------------------------------------

--
-- 表的结构 `bqc_access_token_expires`
--

CREATE TABLE `bqc_access_token_expires` (
  `id` int(10) UNSIGNED NOT NULL,
  `access_token` varchar(700) NOT NULL,
  `expires_in` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `bqc_access_token_expires`
--

INSERT INTO `bqc_access_token_expires` (`id`, `access_token`, `expires_in`) VALUES
(1, 'wVuL9K64PBRzYQ5hVLm0D-OF1gcSs6ed61d5w4AA7FEJeDFJC85rltHmUyAJCMvzYoKo1MVBsMAePf9Sg-jXxyqrnuVkbZ14mUfiYTKsQynNgJ6iF85dMLx6gCXxn2kSBVChABAICJ', 1498984454);

-- --------------------------------------------------------
--
-- 表的结构 `bqc_login_qrcode`
--

CREATE TABLE `bqc_login_qrcode` (
  `id` int(11) UNSIGNED NOT NULL,
  `ticket` varchar(500) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='使用微信登录生成的临时二维码';


--
-- 表的结构 `bqc_member`
--

CREATE TABLE `bqc_member` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL COMMENT '用户昵称',
  `telephone` varchar(11) DEFAULT NULL COMMENT '电话',
  `email` varchar(225) DEFAULT NULL COMMENT '邮箱',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `token` varchar(50) DEFAULT NULL COMMENT '登录用token',
  `refresh_token` varchar(50) DEFAULT NULL COMMENT '刷新用token',
  `salt` char(4) DEFAULT NULL COMMENT '密码盐',
  `third_id` varchar(100) DEFAULT NULL COMMENT '第三方ID',
  `union_id` int(100) DEFAULT NULL COMMENT '微信开放平台id',
  `sex` tinyint(4) NOT NULL DEFAULT '2' COMMENT '性别：1：男；2：女；0：未知',
  `country` varchar(225) DEFAULT NULL COMMENT '国家',
  `province` varchar(225) DEFAULT NULL COMMENT '省',
  `city` varchar(225) DEFAULT NULL COMMENT '市',
  `add_ip` varchar(50) DEFAULT NULL COMMENT '新增时候的ip',
  `last_ip` varchar(50) DEFAULT NULL COMMENT 'ip',
  `avatar` varchar(225) DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态:1；正常；2：删除；3：停用',
  `token_time` int(11) DEFAULT NULL COMMENT '验证token创建时间，有效期2小时',
  `refresh_token_time` int(11) DEFAULT NULL COMMENT '刷新用token创建时间，有效期30天',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `create_time` int(11) NOT NULL COMMENT '创建时间'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `bqc_recognition`
--

CREATE TABLE `bqc_recognition` (
  `id` int(11) NOT NULL,
  `third_type` varchar(30) DEFAULT NULL,
  `third_id` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `ticket` varchar(200) DEFAULT NULL,
  `add_time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Indexes for table `bqc_access_token_expires`
--
ALTER TABLE `bqc_access_token_expires`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bqc_login_qrcode`
--
ALTER TABLE `bqc_login_qrcode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bqc_member`
--
ALTER TABLE `bqc_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bqc_recognition`
--
ALTER TABLE `bqc_recognition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- 使用表AUTO_INCREMENT `bqc_access_token_expires`
--
ALTER TABLE `bqc_access_token_expires`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `bqc_login_qrcode`
--
ALTER TABLE `bqc_login_qrcode`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000000122;
--
-- 使用表AUTO_INCREMENT `bqc_member`
--
ALTER TABLE `bqc_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- 使用表AUTO_INCREMENT `bqc_recognition`
--
ALTER TABLE `bqc_recognition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
